tidy:
	go mod tidy

test_all: test_bufpool

test_bufpool:
	cd bufpool && go test -bench=.
