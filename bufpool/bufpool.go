package bufpool

import (
	"bytes"
	"sync"
	"sync/atomic"
)

type Bufpool struct {
	p sync.Pool

	leased   int64
	returned int64
}

func New() *Bufpool {
	out := &Bufpool{
		p: sync.Pool{
			New: func() interface{} {
				return new(bytes.Buffer)
			},
		},
	}
	return out
}

func (b *Bufpool) Get() *PooledBuffer {
	atomic.AddInt64(&b.leased, 1)
	got := b.p.Get().(*bytes.Buffer)
	got.Reset()
	return &PooledBuffer{
		parent: b,
		Buffer: got,
	}
}

type PooledBuffer struct {
	parent *Bufpool
	*bytes.Buffer
}

func (b *PooledBuffer) Release() {
	atomic.AddInt64(&b.parent.returned, 1)
	b.parent.p.Put(b.Buffer)
}
