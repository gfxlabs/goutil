package bufpool

import (
	"bytes"
	"testing"
)

var payload = []byte("asdgas7tyudg7tyuadgyuiadgyuasgdasgdasgdtyuasgdtyasv dtyjasfdytjasvydjas ydavstyd vajv2j67 dgja3gc 567gc 567tc u3watg c673wafc 563asZ 67jgb c56u3f72349q cb1256cv 235qc gvq23c3werac d53wa bc456wucva458wc4aw6 gc 4w67cv 4qw568g87asyd7yas78da78dy278qd7821786g864872364726368x5468f2gxc48675g3x6784536784536784678c58365c36753785n23465c67956793697956n3489623468n5c34695786n2378526n34rtwerukygf7shf98ufo89wzufl9s37hf78sh35hf 53w69 b569b fv6895b3fw6i fvse57 bgsv5e[ gh[5ehe]5h9[45-9h]s0 gbs40-g0-[4e9sh[9l4z4[y8ma[w7487ytqbo473ahb7v43vb56l3v502 4320mrf5v6wt8wrgef4102q4 5b6rtwglesij1056fqv7 wtl.giyfke951fvg6	 7btoufgkzdej5v6g20-0p746twayruigw3429-qt67ygwruiahjkxt374wy9erusfh346y7tp9eruisfjxnt34-479weruox 389w5t78erdkznm.36789-t57werdkm3t6794ywerusfh679034tqwryuishkzcv604tq239weyuis 7-4516tq3pweisdzn. 40152rt8qyowesdj -6t23479qgwryuisdh6-23t57pwerudfn524604rt3qoersdhk508342w68eyisdf")

func BenchmarkBufpoolRepeatedAlloc(b *testing.B) {
	bufpool := New()
	for i := 0; i < b.N; i++ {
		buf := bufpool.Get()
		buf.Write(payload)
		buf.Write(payload)
		buf.Release()
	}
}

func BenchmarkNaiveRepeatedAlloc(b *testing.B) {
	for i := 0; i < b.N; i++ {
		buf := new(bytes.Buffer)
		buf.Write(payload)
		buf.Write(payload)
	}
}
