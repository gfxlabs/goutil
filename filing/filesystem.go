package filing

import "io/fs"

type HashedFS interface {
	ListFiles() []string
	OpenFile(name string, flag int, perm fs.FileMode) (HashedFile, error)
}

type HashedFile interface {
	Hash() uint64
	Data() ([]byte, error)
	Replace([]byte) error
	Close()
}
