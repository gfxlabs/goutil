package fs_actions

import (
	"errors"
	"log"
	"strings"

	"gitlab.com/gfxlabs/goutil/filing"
	"gitlab.com/gfxlabs/goutil/filing/fs_local"
	"gitlab.com/gfxlabs/goutil/filing/fs_weed"
	"gitlab.com/gfxlabs/goutil/httpclient"
)

type SyncOptions struct {
	Path string
	User string
	Pass string
}

func ConnectFilesystem(s *SyncOptions) filing.HashedFS {
	var out filing.HashedFS
	if strings.HasPrefix(s.Path, "http") {
		client := httpclient.NewAuthClient(s.User, s.Pass)
		out = fs_weed.New(s.Path, client)
	} else if strings.HasPrefix(s.Path, "/") || strings.HasPrefix(s.Path, ".") {
		out = fs_local.New(s.Path)
	}
	return out
}
func SyncFilesystems(src, dst *SyncOptions) error {
	srcFS := ConnectFilesystem(src)
	dstFS := ConnectFilesystem(dst)
	if srcFS == nil {
		return errors.New("invalid src fs:" + src.Path)
	}
	if dstFS == nil {
		return errors.New("invalid dst fs:" + dst.Path)
	}
	return syncFilesystems(srcFS, dstFS)
}

func syncFilesystems(src filing.HashedFS, dst filing.HashedFS) error {
	files := src.ListFiles()
	log.Printf("scanning %d files", len(files))
	for _, v := range files {
		func() {
			srcFile, err := src.OpenFile(v, 0, 0)
			if err != nil {
				log.Println("error opening src file:", v, err)
				return
			}
			defer srcFile.Close()
			dstFile, err := dst.OpenFile(v, 0, 0)
			if err != nil {
				log.Println("error opening dst file:", v, err)
				return
			}
			defer dstFile.Close()
			if srcFile.Hash() != dstFile.Hash() {
				dat, err := srcFile.Data()
				if err != nil {
					log.Println("error reading src file:", v, err)
					return
				}
				err = dstFile.Replace(dat)
				if err != nil {
					log.Println("error reading dst file:", v, err)
					return
				}
			}
		}()
	}
	return nil
}
