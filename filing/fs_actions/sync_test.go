package fs_actions

import (
	"log"
	"os"
	"strings"
	"testing"
)

func TestLocalToLocal(t *testing.T) {
	err := SyncFilesystems(
		&SyncOptions{Path: "../testdata"},
		&SyncOptions{Path: "../testdata_clone"},
	)
	if err != nil {
		log.Println(err)
	}
}

func TestSeaweedToLocal(t *testing.T) {
	url := os.Getenv("FILER_ADDR")
	user := os.Getenv("FILER_USER")
	pass := os.Getenv("FILER_PASS")
	err := SyncFilesystems(
		&SyncOptions{Path: url, User: user, Pass: pass},
		&SyncOptions{Path: "../testdata_fromweed"},
	)
	if err != nil {
		log.Println(err)
	}
}

func TestLocalToSeaweed(t *testing.T) {
	url := strings.ReplaceAll(os.Getenv("FILER_ADDR"), "tmp", "tmp2")
	user := os.Getenv("FILER_USER")
	pass := os.Getenv("FILER_PASS")
	err := SyncFilesystems(
		&SyncOptions{Path: "../testdata"},
		&SyncOptions{Path: url, User: user, Pass: pass},
	)
	if err != nil {
		log.Println(err)
	}
}

func TestSeaweedToSeaweed(t *testing.T) {
	url := strings.ReplaceAll(os.Getenv("FILER_ADDR"), "district", "district_")
	url2 := strings.ReplaceAll(os.Getenv("FILER_ADDR"), "tmp", "tmp2")
	user := os.Getenv("FILER_USER")
	pass := os.Getenv("FILER_PASS")
	err := SyncFilesystems(
		&SyncOptions{Path: url2, User: user, Pass: pass},
		&SyncOptions{Path: url, User: user, Pass: pass},
	)
	if err != nil {
		log.Println(err)
	}
}
