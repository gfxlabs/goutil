package fs_local

import (
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"

	"gitlab.com/gfxlabs/goutil/bufpool"
	"gitlab.com/gfxlabs/goutil/constants"
	"gitlab.com/gfxlabs/goutil/filing"
	"gitlab.com/gfxlabs/meow"
)

var BUFPOOL *bufpool.Bufpool = bufpool.New()

var _ filing.HashedFS = (*FS)(nil)
var _ filing.HashedFile = (*File)(nil)

type FS struct {
	prefix string
}

func New(prefix string) *FS {
	return &FS{prefix: path.Clean(prefix)}
}

func (O *FS) Prefix() string {
	return O.prefix
}
func (O *FS) ListFiles() []string {
	out := make([]string, 0)
	err := filepath.WalkDir(O.prefix, func(p string, info os.DirEntry, err error) error {
		if err != nil {
			fmt.Println("ERROR:", err)
		}
		if info.IsDir() == false {
			out = append(out, strings.Replace(p, O.prefix+"/", "", 1))
		}
		return nil
	})
	if err != nil {
		log.Println(err)
	}
	return out
}

func (O *FS) OpenFile(name string, flag int, perm fs.FileMode) (filing.HashedFile, error) {
	if flag == 0 {
		flag = os.O_RDWR
	}
	if perm == 0 {
		perm = 0740
	}
	err := os.MkdirAll(path.Dir(path.Join(O.prefix, name)), perm)
	if err != nil {
		return nil, err
	}
	a, err := os.OpenFile(path.Join(O.prefix, name), flag|os.O_CREATE, perm)
	if err != nil {
		return nil, err
	}
	buf := BUFPOOL.Get()
	out := &File{
		u:     a,
		store: buf,
	}
	out.Hash()
	out.u.Close()
	if out.hash == 0 {
		return nil, errors.New("Empty File")
	}
	out.perm = perm
	return out, nil
}

type File struct {
	u     *os.File
	store *bufpool.PooledBuffer
	perm  os.FileMode

	hash uint64
}

type FileInfo struct {
	u os.FileInfo
}

func (O *File) Hash() uint64 {
	if O.hash == 0 {
		hasher := meow.New64(constants.THREEBEEF)
		tee := io.TeeReader(O.u, hasher)
		O.store.ReadFrom(tee)
		O.hash = hasher.Sum64()
		return O.hash
	}
	return O.hash
}

func (O *File) Data() ([]byte, error) {
	return O.store.Bytes(), nil
}

func (O *File) Replace(b []byte) error {
	return os.WriteFile(O.u.Name(), b, O.perm)
}
func (O *File) Close() {
	O.store.Release()
}
