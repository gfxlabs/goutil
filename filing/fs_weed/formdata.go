package fs_weed

import (
	"fmt"
	"io"
	"mime/multipart"
	"net/textproto"

	"gitlab.com/gfxlabs/goutil/bufpool"
)

func makeFormData(
	filename, mimeType string,
	content []byte,
) (formData *bufpool.PooledBuffer, err error) {
	buf := BUFPOOL.Get()
	writer := multipart.NewWriter(buf)
	defer writer.Close()
	part, err := createFormFile(writer, "file", filename, mimeType)
	if err != nil {
		return buf, err
	}
	_, err = part.Write(content)
	if err != nil {
		return buf, err
	}
	return buf, err
}

func createFormFile(writer *multipart.Writer, fieldname, filename, mime string) (io.Writer, error) {
	h := make(textproto.MIMEHeader)
	h.Set("Content-Disposition",
		fmt.Sprintf(`form-data; name="%s"; filename="%s"`,
			escapeQuotes(fieldname), escapeQuotes(filename)))
	if len(mime) == 0 {
		mime = "application/octet-stream"
	}
	h.Set("Content-Type", mime)
	return writer.CreatePart(h)
}
