package fs_weed

import (
	"encoding/json"
	"errors"
	"io"
	"io/fs"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"

	"gitlab.com/gfxlabs/goutil/bufpool"
	"gitlab.com/gfxlabs/goutil/constants"
	"gitlab.com/gfxlabs/goutil/filing"
	"gitlab.com/gfxlabs/goutil/httpclient"
	"gitlab.com/gfxlabs/meow"
)

var BUFPOOL = bufpool.New()

const THREEBEEF = 0xbeefbeefbeef

type FS struct {
	prefix string
	client *httpclient.AuthClient

	hash uint64

	directory string
}

type WeedDir struct {
	Entries []struct {
		FullPath string `json:"FullPath"`
		ModTime  string `json:"Mtime"`
		Md5      string `json:"Md5"`
		Extended struct {
			Meow64 string `json:"Seaweed-Meow64"`
		} `json:"Extended"`
	} `json:"Entries"`
	Path string `json:"Path"`
}

func New(prefix string, client *httpclient.AuthClient) *FS {
	out := &FS{prefix: strings.Trim(prefix, "/"), client: client}
	splt := strings.Split(prefix, ".")
	str := splt[len(splt)-1]
	str = strings.SplitN(str, "/", 2)[1]
	out.directory = str
	return out
}

func (O *FS) ListFiles() []string {
	out := make([]string, 0)
	outp := &out
	O.listFiles(O.Prefix(), outp)
	return out
}

func (O *FS) listFiles(folder string, sli *[]string) {
	_, r, err := O.get(folder + "?limit=100000")
	if err != nil {
		return
	}
	var entries WeedDir
	err = json.Unmarshal(r, &entries)
	if err != nil {
		return
	}
	for _, entry := range entries.Entries {
		if entry.Md5 != "" {
			rep := path.Clean(strings.Replace(entry.FullPath, O.directory, "", 1))
			log.Println(rep)
			*sli = append(*sli, rep)
		} else {
			newQuery := strings.Replace(O.Prefix(), O.directory, entry.FullPath, 1)
			O.listFiles(newQuery, sli)
		}
	}
}

func (O *File) updateHash(s string) {
	_, err := O.fs.client.Put(O.path+"?tagging", map[string]string{"Seaweed-Meow64": s})
	if err != nil {
		log.Println("error updating hash:", err)
	}
}

func (F *FS) head(file string) (http.Header, error) {
	head, err := F.client.Head(file)
	if err != nil {
		return nil, err
	}
	return head.Header, nil
}

func (F *FS) get(file string) (http.Header, []byte, error) {
	head, err := F.client.Get(file)
	if err != nil {
		return nil, nil, err
	}
	defer head.Body.Close()
	if head.StatusCode == 200 {
		ob, err := io.ReadAll(head.Body)
		return head.Header, ob, err
	}
	log.Println("filer http error:", head)
	return nil, nil, errors.New("file not found")
}

func (F *FS) push(
	file, content string, dat []byte,
) (http.Header, []byte, error) {
	contentType := content
	if contentType == "" {
		contentType = http.DetectContentType(dat)
	}
	sum := meow.Checksum64(THREEBEEF, dat)
	d, err := makeFormData(normalizeName(file), contentType, dat)
	defer d.Release()
	if err != nil {
		return nil, nil, err
	}
	var url string
	url = file + "?tagging"
	req, err := http.NewRequest(
		"POST",
		url,
		d,
	)
	if err != nil {
		return nil, nil, err
	}
	req.Header.Set("Seaweed-Meow64", strconv.FormatUint(sum, 10))
	req.Header.Set("Content-Type", contentType)
	resp, err := F.client.Do(req)
	if err != nil {
		return nil, nil, err
	}
	if resp.ContentLength > 0 {
		bts, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil, nil, err
		}
		resp.Body.Close()
		return resp.Header, bts, nil
	}
	return resp.Header, nil, nil
}

func (O *FS) Prefix() string {
	return O.prefix
}

func (O *FS) OpenFile(name string, flag int, perm fs.FileMode) (filing.HashedFile, error) {
	out := &File{
		fs:   O,
		path: O.prefix + "/" + strings.Trim(name, "/"),
	}
	header, err := O.head(out.path)
	if err != nil {
		return nil, err
	}
	str := header.Get("Seaweed-Meow64")
	out.hash, err = strconv.ParseUint(str, 10, 64)
	if err != nil {
		if str == "" {
			out.hash = 0
			return out, nil
		}
		return nil, err
	}
	return out, nil
}

type File struct {
	path string

	hash uint64

	fs *FS
}

type FileInfo struct {
	u os.FileInfo
}

func (O *File) Hash() uint64 {
	return O.hash
}

func (O *File) Data() ([]byte, error) {
	header, dat, err := O.fs.get(O.path)
	if err != nil {
		return nil, err
	}
	hash := meow.Checksum64(constants.THREEBEEF, dat)
	hashstring := header.Get("Seaweed-Meow64")
	O.hash, err = strconv.ParseUint(hashstring, 10, 64)
	if O.hash != hash {
		O.updateHash(strconv.FormatUint(hash, 10))
	}
	return dat, nil
}

func (O *File) Replace(b []byte) error {
	_, _, err := O.fs.push(O.path, http.DetectContentType(b), b)
	return err
}

func (O *File) Close() {
	return
}
