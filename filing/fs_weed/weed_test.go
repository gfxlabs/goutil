package fs_weed

import (
	"log"
	"os"
	"testing"

	"gitlab.com/gfxlabs/goutil/httpclient"
)

func TestHashUnicode(t *testing.T) {
	url := os.Getenv("FILER_ADDR")
	user := os.Getenv("FILER_USER")
	pass := os.Getenv("FILER_PASS")

	client := httpclient.NewAuthClient(user, pass)
	filer := New(url, client)

	log.Println(filer.ListFiles())

	wow, err := filer.OpenFile("narwhal.png", 0, 0)
	if err != nil {
		t.Fatalf("failed to open file: %s", err)
	}
	defer wow.Close()
	wow2, err := filer.OpenFile("narwhal2.png", 0, 0)
	if err != nil {
		t.Fatalf("failed to open file: %s", err)
	}
	defer wow.Close()
	wowhash := wow.Hash()
	wow2hash := wow2.Hash()
	if wowhash == 0 || wow2hash == 0 {
		t.Fatalf("expected hash %d and %d to not be 0", wowhash, wow2hash)
	}
	if wowhash != wow2hash {
		t.Fatalf("expected hash %d to equal %d", wowhash, wow2hash)
	}

	wowdat, err := wow.Data()
	if err != nil {
		t.Fatalf("failed to read data for wow")
	}
	wow2dat, err := wow2.Data()
	if err != nil {
		t.Fatalf("failed to read data for wow2")
	}

	if len(wowdat) == 0 || len(wow2dat) == 0 {
		t.Fatalf("expected data %s and %s to not be len 0", wowdat, wow2dat)
	}

	if string(wowdat) != string(wow2dat) {
		t.Fatalf("expected data %s to equal %s", wowdat, wow2dat)
	}

}
