package httpclient

import (
	"net/http"
)

type AuthClient struct {
	user   string
	key    string
	client *http.Client
}

func NewAuthClient(user, key string) *AuthClient {
	return &AuthClient{
		user:   user,
		key:    key,
		client: &http.Client{},
	}
}

func (A *AuthClient) Do(req *http.Request) (*http.Response, error) {
	req.Header.Add("GFX-API-TOKEN", A.key)
	req.Header.Add("GFX-API-USER", A.user)
	req.Header.Add("Accept", "application/json")
	return A.client.Do(req)
}

func (A *AuthClient) Head(url string) (*http.Response, error) {
	req, err := http.NewRequest("HEAD", url, nil)
	if err != nil {
		return nil, err
	}
	return A.Do(req)
}

func (A *AuthClient) Get(url string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	return A.Do(req)
}
func (A *AuthClient) Put(url string, headers map[string]string) (*http.Response, error) {
	req, err := http.NewRequest("PUT", url, nil)
	if err != nil {
		return nil, err
	}
	for k, v := range headers {
		req.Header.Set(k, v)
	}
	return A.Do(req)
}
