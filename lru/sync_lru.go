package lru

import (
	"container/list"
	"sync"
	"time"
)

type ConcurrentLRU struct {
	u *LRU
	sync.RWMutex
}

// NewConcurrentLRU constructs an ConcurrentLRU of the given size
func NewConcurrentLRU(size int, onEvict EvictCallback) (*ConcurrentLRU, error) {
	c := &ConcurrentLRU{}
	var err error
	c.u, err = NewLRU(size, onEvict)
	return c, err
}

// NewConcurrentLRUWithExpire contrusts an ConcurrentLRU of the given size and expire time
func NewConcurrentLRUWithExpire(size int, expire time.Duration, onEvict EvictCallback) (*ConcurrentLRU, error) {
	c := &ConcurrentLRU{}
	var err error
	c.u, err = NewLRUWithExpire(size, expire, onEvict)
	return c, err
}

// Purge is used to completely clear the cache
func (c *ConcurrentLRU) Purge() {
	c.Lock()
	defer c.Unlock()
	c.u.Purge()
}

// Add adds a value to the cache.  Returns true if an eviction occurred.
func (c *ConcurrentLRU) Add(key, value interface{}) bool {
	c.Lock()
	defer c.Unlock()
	return c.u.Add(key, value)
}

// AddEx adds a value to the cache with expire.  Returns true if an eviction occurred.
func (c *ConcurrentLRU) AddEx(key, value interface{}, expire time.Duration) bool {
	c.Lock()
	defer c.Unlock()
	return c.u.AddEx(key, value, expire)
}

// Get looks up a key's value from the cache.
func (c *ConcurrentLRU) Get(key interface{}) (value interface{}, ok bool) {
	c.Lock()
	defer c.Unlock()
	return c.u.Get(key)
}

// Check if a key is in the cache, without updating the recent-ness
// or deleting it for being stale.
func (c *ConcurrentLRU) Contains(key interface{}) (ok bool) {
	c.Lock()
	defer c.Unlock()
	return c.u.Contains(key)
}

// Returns the key value (or undefined if not found) without updating
// the "recently used"-ness of the key.
func (c *ConcurrentLRU) Peek(key interface{}) (value interface{}, ok bool) {
	c.Lock()
	defer c.Unlock()
	return c.u.Peek(key)
}

// Remove removes the provided key from the cache, returning if the
// key was contained.
func (c *ConcurrentLRU) Remove(key interface{}) bool {
	c.Lock()
	defer c.Unlock()
	return c.u.Remove(key)
}

// RemoveOldest removes the oldest item from the cache.
func (c *ConcurrentLRU) RemoveOldest() (interface{}, interface{}, bool) {
	c.Lock()
	defer c.Unlock()
	return c.u.RemoveOldest()
}

// GetOldest returns the oldest entry
func (c *ConcurrentLRU) GetOldest() (interface{}, interface{}, bool) {
	c.Lock()
	defer c.Unlock()
	return c.u.GetOldest()

}

// Keys returns a slice of the keys in the cache, from oldest to newest.
func (c *ConcurrentLRU) Keys() []interface{} {
	c.Lock()
	defer c.Unlock()
	return c.u.Keys()
}

// Len returns the number of items in the cache.
func (c *ConcurrentLRU) Len() int {
	c.Lock()
	defer c.Unlock()
	return c.u.Len()
}

// Resize changes the cache size.
func (c *ConcurrentLRU) Resize(size int) (evicted int) {
	c.Lock()
	defer c.Unlock()
	return c.u.Resize(size)
}

// removeOldest removes the oldest item from the cache.
func (c *ConcurrentLRU) removeOldest() {
	c.Lock()
	defer c.Unlock()
	c.u.removeOldest()
}

// removeElement is used to remove a given list element from the cache
func (c *ConcurrentLRU) removeElement(e *list.Element) {
	c.Lock()
	defer c.Unlock()
	c.u.removeElement(e)
}
